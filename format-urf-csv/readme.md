# URF CSV Format

Parser and serializer for the the URF CSV document format.

URF CSV is an import format. It does not support the full URF data model and allows columns to be ignored.

## Download

URF CSV Format is available in the Maven Central Repository as [io.urf:format-urf-csv](https://search.maven.org/search?q=g:io.urf%20AND%20a:format-urf-csv).

## Issues

Issues tracked by [JIRA](https://globalmentor.atlassian.net/projects/URF).

## Changelog

- 0.3.0: Initial release.
